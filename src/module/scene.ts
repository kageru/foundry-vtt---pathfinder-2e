import { TokenDocumentPF2e } from './token-document';

export type ScenePF2e = Scene<TokenDocumentPF2e>;
