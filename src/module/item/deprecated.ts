import { ItemPF2e } from './base';

/** Deprecate item types, kept until there is a way to safely remove them from the system */

export class MartialPF2e extends ItemPF2e {}
